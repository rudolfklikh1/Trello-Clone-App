import { TestBed } from '@angular/core/testing';

import { BoardService } from './board.service';
import { SocketService } from 'src/app/shared/services/socket.service';
import { HttpClientModule } from '@angular/common/http';

describe('BoardService', () => {
  let service: BoardService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [BoardService, SocketService],
    });
    service = TestBed.inject(BoardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
