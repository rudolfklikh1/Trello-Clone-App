import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardsComponent } from './boards.component';
import { BoardsService } from 'src/app/shared/services/boards.service';
import { HttpClientModule } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('BoardsComponent', () => {
  let component: BoardsComponent;
  let fixture: ComponentFixture<BoardsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BoardsComponent],
      imports: [HttpClientModule],
      providers: [BoardsService],
      schemas: [NO_ERRORS_SCHEMA],
    });
    fixture = TestBed.createComponent(BoardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
